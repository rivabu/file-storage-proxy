package com.univentio.lnip.storageproxy.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.univentio.lnip.storageproxy.domain.FileContent;

/**
 * The Class StorageService.
 */
@Component
public class StorageService  {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(StorageService.class);
    
    @Value("${rootdir}")
    String rootDir;
     

	public void addFile(FileContent fileContent) throws IOException {
        FileUtils.writeByteArrayToFile(new File(rootDir + "/" + fileContent.getDirectory() + "/" +  fileContent.getName()), fileContent.getContent().getBytes(Charset.forName("UTF-8")));
	}
	
	public FileContent getFile(final String directory, final String name) throws IOException {
		FileContent fc = new FileContent();
		fc.setName(name);
		fc.setBinary(false);
		fc.setContent(new String(Files.readAllBytes(Paths.get(rootDir + "/" + directory + "/" +  name))));
		return fc;
	}

}
