package com.univentio.lnip.storageproxy.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.univentio.lnip.storageproxy.domain.FileContent;
import com.univentio.lnip.storageproxy.services.StorageService;
import com.wordnik.swagger.annotations.Api;

/**
 * The Class FileController.
 */
@PropertySource("classpath:application.properties")

@RestController
@RequestMapping("/file")
@Api(value = "file", description = "Summit FileStorageProxy REST API")
public class FileController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class);

    
    @Autowired
	private StorageService storageService;
    
    @RequestMapping(value = "/put", method = RequestMethod.POST)
	public ResponseEntity<?> addFile( @RequestBody FileContent fileContent) throws MethodArgumentNotValidException {
		try {
			storageService.addFile(fileContent);
		} catch (IOException e) {
			LOGGER.error("problems saving file: {}", e);
		}
		return new ResponseEntity(HttpStatus.OK);

	}    
    
    @RequestMapping(value = "/get/{directory}/{name}", method = RequestMethod.GET)
	public ResponseEntity<FileContent> getFile(@PathVariable("directory") String directory, @PathVariable("name") String name) {
		
		FileContent file = null;
		try {
			file = storageService.getFile(directory, name);
		} catch (IOException e) {
			LOGGER.error("problems reading file: {}", e);
		}
		ResponseEntity<FileContent> response = new ResponseEntity<FileContent>(file, HttpStatus.OK);
		return response;
	}

}
