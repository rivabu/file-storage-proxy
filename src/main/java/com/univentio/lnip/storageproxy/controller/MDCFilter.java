package com.univentio.lnip.storageproxy.controller;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Component
@WebFilter( filterName = "mdcFilter", urlPatterns = { "/*" } )
public class MDCFilter implements Filter {

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter( final ServletRequest request, final ServletResponse response, final FilterChain chain )
        throws IOException, ServletException {


        // Get the parameter value.
        String correlationId = ((HttpServletRequest) request).getHeader("correlationId");
        if (StringUtils.isBlank(correlationId)) {
            correlationId = "serverside-generated-"+ UUID.randomUUID().toString();
        }
        // Put it in the MDC map.
        MDC.put( "correlation_id", correlationId );

        try {
            chain.doFilter( request, response );
        } finally {
            // When the control returns to the filter, clean it.
            MDC.remove( "correlation_id" );
        }
    }

    @Override
    public void init( final FilterConfig filterConfig ) throws ServletException {

    }
}
