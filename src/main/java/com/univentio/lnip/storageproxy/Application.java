package com.univentio.lnip.storageproxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * The Class Application. This is the main class!
 */
@SpringBootApplication
@PropertySource("classpath:application.properties")
public class Application {
	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	/**
	 * The main method. Here it all starts
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
